package cat.itb.m05.uf2

import org.junit.jupiter.api.Test

import org.junit.jupiter.api.Assertions.*

internal class SomeRandomFunctionsKtTest {

    @Test

    fun needFunctionName() {
        val input = cat.itb.m05.uf2.needFunctionName(3,7) //Introdueixes valors "INT"
        val output = mutableListOf<Int>(3,4,5,6,7)  //Esperes en resposta una llista
        assertEquals(input,output)
    }

    @Test
    fun minValue() {
        val expected = -3
        assertEquals(expected, minValue(mutableListOf<Int>(7,5,9,-3)))


    }




    @Test
    fun select() {
        fun cualificacions() {  // introdueixes un valor int i fas return
            val numcualificacio = 1
            val cualificacio:String

            when(numcualificacio){
                1, 2, 3, 4 -> cualificacio = "Insuficient"   //return
                0 -> cualificacio = "No presentat"
                10 -> cualificacio = "Excelent + MH"
                5, 6 -> cualificacio = "Suficient"
                7 -> cualificacio = "Notable"
                else -> cualificacio = "No valid"
            }
            assert(cualificacio == "Insuficient")    //verifica que el
        }



    }

    @Test
    fun charPositionsInString() {
//        val si = "papa"
//        val b = "a"
//        val z = mutableListOf<Int>()
//        var coincidencies = 0
//        for (i in 0 < until < si.lastIndex){
//            if (si[i] == b){
//                z.add()
//            }
//        }
    }


    @Test
    fun firstOccurrencePosition() {
//        val pos: MutableList<Int> = mutableListOf()
//        val word = "hola"
//        val letter = 'a'
//        for (i in 0 ≤ until word.length){
//            if (word[i] == letter){
//                return i
//            }
//        }
//



    }

    @Test
    fun note() {
        val expected : Double=7.7
        assert(expected.toInt()==7)
    }
}

